// OFF CANVAS INTERACTIONS
function closeNav() {
  document.getElementById("page-wrap").classList.remove('show-nav');
}
function toggleNav() {
  page_wrapper = document.getElementById("page-wrap");
  if(page_wrapper.classList.contains('show-nav')) {
    page_wrapper.classList.remove('show-nav');
  } else {
    page_wrapper.classList.add('show-nav');
  }
}
function toggleSearchBar() {
  search_bar = document.getElementById("search-drop");
  marker = document.getElementById("search--marker");
  if(search_bar.classList.contains('show-search')) {
    marker.classList.add('noview');
    search_bar.classList.remove('show-search');
  } else {
    search_bar.classList.add('show-search');
    document.getElementById("search-drop").addEventListener("transitionend", function() {
      if(search_bar.classList.contains('show-search')) {
        marker.classList.remove('noview');
      }
    });
  }
}
// FOUC
document.addEventListener("DOMContentLoaded",function() {
  document.documentElement.classList.remove('no-fouc');
});