from django.contrib.auth import authenticate, login
from django.template.response import TemplateResponse
from django.shortcuts import redirect
from django.template.loader import get_template

def loginview(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('/')
    else:
        res = TemplateResponse(request, get_template('401.html'), status=401, charset="UTF-8")
        return res
        #invalid login message
