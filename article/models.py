from django.db import models

from modelcluster.fields import ParentalKey

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import StreamField
from wagtail.wagtailcore import blocks
from wagtail.wagtailadmin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel, InlinePanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailimages.blocks import ImageChooserBlock

# Create your models here.
class ArticlePage(Page):
    #Database fields
    headline = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    date = models.DateField("Publication date")
    body = StreamField([
        ('heading', blocks.CharBlock(classname="story-lead")),
        ('paragraph', blocks.RichTextBlock()),
        ('image', ImageChooserBlock()),
    ])
    post_image = models.ForeignKey('wagtailimages.Image',
                                   null=True,
                                   blank=True,
                                   on_delete=models.SET_NULL,
                                   related_name='+'
                                  )

    #Editor panel config
    content_panels = Page.content_panels + [
        FieldPanel('headline'),
        FieldPanel('author'),
        FieldPanel('date'),
        StreamFieldPanel('body'),
        InlinePanel('gallery_images', label="Gallery images"),
        InlinePanel('related_links', label="Related links"),
    ]
    promote_panels = [
        MultiFieldPanel(Page.promote_panels, "Common page configuration"),
        ImageChooserPanel('post_image'),
    ]


class ArticlePageRelatedLink(Orderable):
    #Database fields
    page = ParentalKey(ArticlePage, related_name='related_links')
    name = models.CharField(max_length=255)
    url = models.URLField()

    #Editor panel config 
    panels = [
        FieldPanel('name'),
        FieldPanel('url'),
    ]

class ArticlePageGalleryImage(Orderable):
    #Database fields
    page = ParentalKey(ArticlePage, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    #Editor panel config 
    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]

