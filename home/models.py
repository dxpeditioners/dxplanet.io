from __future__ import absolute_import, unicode_literals

from django.db import models
from modelcluster.fields import ParentalKey
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel

from article.models import ArticlePage

class HomePage(Page):
    #Database fields
    #Allow hiding unpublished content
    def get_context(self, request):
        # Update context to include only published posts, ordered by reverse-chron
        context = super(HomePage, self).get_context(request)
        articles = self.get_children().live().type(ArticlePage)
        context['articles'] = articles
        return context 
    #Parent/Sub- page type specs
    parent_page_types = []
    subpage_types = ['article.ArticlePage']
