from django.db import models

from modelcluster.fields import ParentalKey

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index

class BlogIndexPage(Page):
    #Database fields
    intro = RichTextField(blank=True)

    #Editor panel config
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]

    #Allow hiding unpublished content
    def get_context(self, request):
        # Update context to include only published posts, ordered by reverse-chron
        context = super(BlogIndexPage, self).get_context(request)
        blogpages = self.get_children().live().order_by('-first_published_at')
        context['blogpages'] = blogpages
        return context
   
    parent_page_types = []
    subpage_types = ['blog.BlogPage']

class BlogPage(Page):
    #Database fields
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    post_image = models.ForeignKey('wagtailimages.Image',
                                   null=True,
                                   blank=True,
                                   on_delete=models.SET_NULL,
                                   related_name='+'
                                  )

    #Search index config
    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
        index.FilterField('date'),
    ]

    #Editor panel config
    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('intro'),
        FieldPanel('body', classname="full"),
        InlinePanel('gallery_images', label="Gallery images"),
        InlinePanel('related_links', label="Related links"),
    ]
    promote_panels = [
        MultiFieldPanel(Page.promote_panels, "Common page configuration"),
        ImageChooserPanel('post_image'),
    ]

    #Parent/Sub- page type specs
    parent_page_types = ['blog.BlogIndexPage']
    subpage_types = []


class BlogPageRelatedLink(Orderable):
    #Database fields
    page = ParentalKey(BlogPage, related_name='related_links')
    name = models.CharField(max_length=255)
    url = models.URLField()

    #Editor panel config 
    panels = [
        FieldPanel('name'),
        FieldPanel('url'),
    ]

class BlogPageGalleryImage(Orderable):
    #Database fields
    page = ParentalKey(BlogPage, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    #Editor panel config 
    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]
